<%@ page import="com.nesciotamen.model.Status" %><%--
  Created by IntelliJ IDEA.
  User: nesciotamen
  Date: 2019-07-21
  Time: 15:17
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<jsp:include page="/header.jsp"></jsp:include>

<%-- Formularz dodawania oceny. --%>
<form action="/checklistItem/add" method="post">
    Item: <input type="hidden" name="id" value="<%=request.getAttribute("id")%>" readonly>

    <input type="text" name="name" placeholder="Checklist Name">
    <input type="text" name="description" placeholder="Checklist Description">
    <select name="status">
        <%
            for (Status value : Status.values()) {
                out.println("<option value=" + "\"" + value + "\"" + ">" + value + "</option>");
            }

        %>
    </select>
    <br>
    <br>
    <input type="submit" value="Submit">

</form>
</body>
</html>
