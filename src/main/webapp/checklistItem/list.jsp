<%@ page import="com.nesciotamen.model.ChecklistItem" %>
<%@ page import="java.util.List" %><%--
  Created by IntelliJ IDEA.
  User: nesciotamen
  Date: 2019-07-21
  Time: 21:41
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<jsp:include page="/header.jsp"></jsp:include>
<table>
    <tr>
        <th style="width: 150px;">Name</th>
        <th style="width: 150px;">Description</th>
        <th style="width: 150px;">Status</th>
    </tr>
    <%
        List<ChecklistItem> checklistsitems = (List<ChecklistItem>) request.getAttribute("checklistItem");

        for (ChecklistItem checklistItem : checklistsitems) {
            out.print("<tr>");
            out.print("<td>" + checklistItem.getName() + "</td>");
            out.print("<td>" + checklistItem.getDescription() + "</td>");
            out.print("<td>" + checklistItem.getStatus() + "</td>");
            out.print("</tr>");
        }

    %>
</table>
</body>
</html>
