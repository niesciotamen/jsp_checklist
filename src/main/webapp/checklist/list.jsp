<%@ page import="com.nesciotamen.model.Checklist" %>
<%@ page import="java.util.List" %><%--
  Created by IntelliJ IDEA.
  User: nesciotamen
  Date: 2019-07-21
  Time: 14:51
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Chcecklist List</title>
</head>
<body>
<jsp:include page="/header.jsp"></jsp:include>
<table>
    <tr>
        <th style="width: 150px;">Name</th>
        <th style="width: 150px;">Date Created</th>
        <th style="width: 150px;">Date Completed</th>
        <th style="width: 150px;">Archived</th>
        <th style="width: 150px;">Remove</th>
        <th style="width: 150px;">All Items</th>
        <th style="width: 150px;">Add Item to list</th>
    </tr>
    <%
        List<Checklist> checklists = (List<Checklist>) request.getAttribute("checklist");

        for (Checklist checklist : checklists) {
            out.print("<tr>");
            out.print("<td>" + checklist.getName() + "</td>");
            out.print("<td>" + checklist.getDateCreated() + "</td>");
            out.print("<td>" + checklist.getDateCompleted() + "</td>");
            out.print("<td>" + checklist.isArchived() + "</td>");
            out.print("<td>" + "<a href=\"/checklist/remove?id=" + checklist.getId() + "\">" + "Remove Checklist</a>" + "</td>");
            out.print("<td>" + "<a href=\"/checklistItem/list?id=" + checklist.getId() + "\">" + "ChecklistItem</a>" + "</td>");
            out.print("<td>" + "<a href=\"/checklistItem/add?id=" + checklist.getId() + "\">" + "Add Item to list</a>" + "</td>");
            out.print("</tr>");
        }

    %>
</table>

</body>
</html>
