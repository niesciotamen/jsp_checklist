package com.nesciotamen.database;

import org.hibernate.SessionFactory;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

public class DatabaseSession {
    private static SessionFactory sessionFactory;

    static {
        // zaĹaduje siÄ jeden raz przy zaĹadowaniu klasy
        // Tworzymy sobie obiekt, ktĂłry pobiera konfiguracjÄ z pliku hibernate cfg xml
        StandardServiceRegistry standardServiceRegistry =
                new StandardServiceRegistryBuilder()
                        .configure("hibernate.cfg.xml").build();

        // metadata to informacje dotyczÄce plikĂłw. z danych zaĹadowanych wczeĹniej
        // towrzymy sobie obiekt metadata.
        Metadata metadata = new MetadataSources(standardServiceRegistry)
                .getMetadataBuilder().build();


        // stworzenie sesji z danych zawartych w pliku hibernate cfg xml
        sessionFactory = metadata.getSessionFactoryBuilder().build();
    }

    public static SessionFactory getSessionFactory() {
        return sessionFactory;
    }
}
