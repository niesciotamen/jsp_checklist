package com.nesciotamen.model;

public interface IBaseEntity {
    Long getId();
}
