package com.nesciotamen.model;

public enum Status {
    TO_DO,
    DONE,
    IN_PROGRESS;
}
