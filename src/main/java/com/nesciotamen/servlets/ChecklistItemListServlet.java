package com.nesciotamen.servlets;

import com.nesciotamen.database.EntityDao;
import com.nesciotamen.model.Checklist;
import com.nesciotamen.model.ChecklistItem;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Optional;

@WebServlet("/checklistItem/list")
public class ChecklistItemListServlet extends HttpServlet {
    private EntityDao entityDao = new EntityDao();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        final String id = req.getParameter("id");

        if (id != null) {
            final Optional<Checklist> optionalChecklist = entityDao.findById(Checklist.class, Long.parseLong(id));
            if (optionalChecklist.isPresent()) {
                req.setAttribute("checklistItem", optionalChecklist.get().getChecklistItems());
            } else {
                System.err.println("Not found this checklist Items");
            }
        } else {
            req.setAttribute("checklistItem", entityDao.findAll(ChecklistItem.class));
        }

        req.getRequestDispatcher("/checklistItem/list.jsp").forward(req, resp);

    }
}
