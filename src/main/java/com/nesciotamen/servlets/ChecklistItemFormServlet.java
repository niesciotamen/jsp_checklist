package com.nesciotamen.servlets;

import com.nesciotamen.database.EntityDao;
import com.nesciotamen.model.Checklist;
import com.nesciotamen.model.ChecklistItem;
import com.nesciotamen.model.Status;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Optional;

@WebServlet("/checklistItem/add")
public class ChecklistItemFormServlet extends HttpServlet {
    private EntityDao entityDao = new EntityDao();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        final String id = req.getParameter("id");

        req.setAttribute("id", id);
        req.getRequestDispatcher("/checklistItem/form.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        ChecklistItem checklistItem = new ChecklistItem();
        final String id = req.getParameter("id");

        checklistItem.setName(req.getParameter("name"));
        checklistItem.setDescription(req.getParameter("description"));
        checklistItem.setStatus(Status.valueOf(req.getParameter("status").toUpperCase()));

        final Optional<Checklist> checkListOptional = entityDao.findById(Checklist.class, Long.parseLong(id));
        if (checkListOptional.isPresent()) {
            Checklist checklist =checkListOptional.get();
            checklistItem.setChecklist(checklist);

            entityDao.saveOrUpdate(checklistItem);
            checklist.getChecklistItems().add(checklistItem);
            entityDao.saveOrUpdate(checklist);
        }

        entityDao.saveOrUpdate(checklistItem);
        resp.sendRedirect("/checklistItem/list");

    }
}
