package com.nesciotamen.servlets;

import com.nesciotamen.database.EntityDao;
import com.nesciotamen.model.Checklist;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/checklist/add")
public class ChecklistFormServlet extends HttpServlet {
    private EntityDao entityDao = new EntityDao();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("/checklist/form.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        final String name = req.getParameter("name");
        Checklist checklist = new Checklist(name);

        entityDao.saveOrUpdate(checklist);

        resp.sendRedirect("/checklist/list");
    }
}
