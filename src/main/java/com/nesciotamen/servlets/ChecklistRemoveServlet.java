package com.nesciotamen.servlets;

import com.nesciotamen.database.EntityDao;
import com.nesciotamen.model.Checklist;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/checklist/remove")
public class ChecklistRemoveServlet extends HttpServlet {
    private EntityDao entityDao = new EntityDao();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Long id = Long.parseLong(req.getParameter("id"));

        entityDao.removeById(Checklist.class, id);

        resp.sendRedirect("/checklist/list");
    }
}
