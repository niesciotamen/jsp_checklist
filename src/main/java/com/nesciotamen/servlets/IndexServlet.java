package com.nesciotamen.servlets;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/index")
public class IndexServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        // wewnÄtrz metody getRequestDispatcher wpisujemy adres pliku "jsp" (widoku)
        // request Dispatcher Ĺaduje template'y

        req.getRequestDispatcher("/index.jsp").forward(req, resp);
    }
}
